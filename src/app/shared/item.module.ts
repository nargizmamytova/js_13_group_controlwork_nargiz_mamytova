export class Item {
  constructor(
    public name: string,
    public imageUrl: string,
    public coast: number,
    public price: number
  ) {}

}

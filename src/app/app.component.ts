import {Component, EventEmitter, Output} from '@angular/core';
import {Item} from "./shared/item.module";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @Output() createOrder = new EventEmitter<Item>();
  AllItems: Item[] = [
    new Item("Hamburger", '', 0, 120),
    new Item("Fri", '', 0, 50),
    new Item("CheeseBurger", '', 0, 150),
    new Item("Cola", '', 0, 60),
    new Item("Coffee", '', 0, 90),
    new Item("Tea", '', 0, 20),
 ]
  onCreateItem(item: Item) {
    this.AllItems.push(item);
  }
  newOrder = [];
  onCreateOrder(i: number){
    this.createOrder.emit(this.newOrder[i]);

  }

}

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Item} from "../shared/item.module";

@Component({
  selector: 'app-add-items',
  templateUrl: './add-items.component.html',
  styleUrls: ['./add-items.component.css']
})
export class AddItemsComponent {
  @Output() createItem = new EventEmitter<Item>();
  @Input() item! : Item;
  @Input() allItems :Item[] = [];

  createNewItem(i: number) {
    this.allItems[i].coast += 1;

  }
}


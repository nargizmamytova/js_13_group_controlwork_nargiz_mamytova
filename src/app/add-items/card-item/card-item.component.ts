import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-card-item',
  templateUrl: './card-item.component.html',
  styleUrls: ['./card-item.component.css']
})
export class CardItemComponent {
  @Input() name: string = '';
  @Input() img: string = '';
  @Input() coast: number = 0;
  @Input() price: number = 0;
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { AddItemsComponent } from './add-items/add-items.component';
import { CardItemComponent } from './add-items/card-item/card-item.component';
import {FormsModule} from "@angular/forms";
import { OrderComponent } from './order-details/order/order.component';

@NgModule({
  declarations: [
    AppComponent,
    OrderDetailsComponent,
    AddItemsComponent,
    CardItemComponent,
    OrderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

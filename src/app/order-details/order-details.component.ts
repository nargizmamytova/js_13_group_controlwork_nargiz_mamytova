import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Item} from "../shared/item.module";

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent {
  @Input() name: string = '';
  @Input() img: string = '';
  @Input() coast: number = 0;
  @Input() price: number = 0;
  @Input() orders: Item[] = [];

  onSumOrder(i: number) {
    this.price = this.coast* this.price
  }
}

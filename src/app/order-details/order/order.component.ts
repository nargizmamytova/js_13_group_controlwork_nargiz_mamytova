import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent{
@Input() ordersName = '';
@Input() ordersCoast = 0;
@Input() ordersPrice = 0 ;
@Output() sumOrder = new EventEmitter();

  onSumPrice() {
    this.sumOrder.emit(this.ordersPrice);

  }
}
